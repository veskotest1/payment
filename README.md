# README #

*****EMERCHANTPAY PAYMENTS*****

### What is this repository for? ###

Merchants and transations gateway

### How do I get set up? ###

Spring boot application

### Technology usage ###

PostgreSQL
Hibernate ORM
Spring Data
Spring MVC
Spring Security
JUnit
Mockito
PMD

Cron job for deleting old transactions

### NOTE ###
All credentials and properties can be get from application.properties