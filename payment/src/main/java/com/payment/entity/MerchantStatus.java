package com.payment.entity;

public enum MerchantStatus {
	ACTIVE,
	INACTIVE
}
