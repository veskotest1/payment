package com.payment.entity;

public enum TransactionStatus {
	APPROVED,
	REVERSED,
	REFUNDED,
	ERROR
}
