package com.payment.service;

import java.util.List;
import java.util.Optional;

import com.payment.entity.Merchant;

public interface MerchantService {
	public List<Merchant> getAllMerchants();

	public void create(Merchant merchant);

	public Optional<Merchant> getById(long id);

	public Optional<Merchant> update(Merchant merchant, long id);

	public void deleteById(long id);
}
