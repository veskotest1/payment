package com.payment.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.payment.entity.Transaction;

public interface TransactionService {

	public List<Transaction> getAllTransactions();

	public Optional<Transaction> getByUuid(UUID id);

	public void create(Transaction trn);

	public void deleteById(UUID id);

	public void deleteOldTrns();

}
