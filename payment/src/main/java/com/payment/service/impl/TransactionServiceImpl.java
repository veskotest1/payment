package com.payment.service.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payment.entity.Transaction;
import com.payment.repository.TransactionRepository;
import com.payment.service.TransactionService;
import com.payment.validation.TransactionValidation;

@Service
@SuppressWarnings("PMD")
public class TransactionServiceImpl implements TransactionService {

	private Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

	@Autowired
	private TransactionRepository repository;

	@Autowired
	private TransactionValidation validation;

	@Override
	public List<Transaction> getAllTransactions() {
		logger.info("Find all transactions");
		return repository.findAll();
	}

	@Override
	public Optional<Transaction> getByUuid(UUID id) {
		logger.info("Get trn with id: {}", id);
		return repository.findById(id);
	}

	@Override
	public void create(Transaction trn) {
		validation.validationTrn(trn);
		repository.save(trn);
		logger.info("Trn with id {} has been created successful!", trn.getId());
	}

	@Override
	public void deleteById(UUID id) {
		repository.deleteById(id);
		logger.info("Trn {} has been deleted.", id);
	}

	@Override
	public void deleteOldTrns() {
		List<Transaction> allTransactions = getAllTransactions();
		Timestamp currentTime = new Timestamp(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(1));
		for (Transaction trn : allTransactions) {
			if (currentTime.after(trn.getPublishDate())) {
				deleteById(trn.getId());
				logger.info("Trn {} has been deleted.", trn.getId());
			}
		}
	}

	public void setRepository(TransactionRepository repo) {
		this.repository = repo;

	}
}
