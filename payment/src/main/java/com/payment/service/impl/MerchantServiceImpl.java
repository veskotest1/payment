package com.payment.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payment.entity.Merchant;
import com.payment.repository.MerchantRepository;
import com.payment.service.MerchantService;

@Service
@SuppressWarnings("PMD")
public class MerchantServiceImpl implements MerchantService {

	private Logger logger = LoggerFactory.getLogger(MerchantServiceImpl.class);

	@Autowired
	private MerchantRepository repository;

	@Override
	public List<Merchant> getAllMerchants() {
		logger.info("Find all merchants");
		return repository.findAll();
	}

	@Override
	public void create(Merchant merchant) {
		repository.save(merchant);
		logger.info("Merchant with id {} has been created successful!", merchant.getId());
	}

	@Override
	public Optional<Merchant> getById(long id) {
		logger.info("Get merchant with id: {}", id);
		return repository.findById(id);
	}

	@Override
	public Optional<Merchant> update(Merchant merchant, long id) {
		Optional<Merchant> merc = repository.findById(id).map(m -> {
			m.setName(merchant.getName());
			m.setDescription(merchant.getDescription());
			m.setEmail(merchant.getEmail());
			m.setStatus(merchant.getStatus());
			m.setTotalTransactionSum(merchant.getTotalTransactionSum());
			return repository.save(m);
		});
		logger.info("Merchant with id {} has been updated.", id);
		return merc;

	}

	@Override
	public void deleteById(long id) {
		repository.deleteById(id);
		logger.info("Merchant {} has been deleted.", id);
	}

	public void setRepository(MerchantRepository repo) {
		this.repository = repo;

	}
}
