package com.payment.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.payment.service.TransactionService;

@Component
@SuppressWarnings("PMD")
public class TransactionJob {

	@Autowired
	private TransactionService service;

	// Start job every hour at minute 0
	@Scheduled(cron = "0 0 * * * *")
	public void deleteOldTrns() {
		service.deleteOldTrns();
	}
}
