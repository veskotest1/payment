package com.payment.validation;

import java.math.BigDecimal;
import java.util.regex.PatternSyntaxException;

import org.springframework.stereotype.Component;

import com.payment.entity.Transaction;
import com.payment.entity.TransactionStatus;

@Component
public class TransactionValidation {

	private static final String UUID_PATTERN = "^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$";
	private static final String EMAIL_PATTERN = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

	public void validationTrn(Transaction trn) {
		if (trn.getId() != null && !trn.getId().toString().matches(UUID_PATTERN)) {
			throw new PatternSyntaxException("Uuid is invalid sytax.", UUID_PATTERN, -1);
		}
		if (trn.getAmount() != null && trn.getAmount().compareTo(BigDecimal.ZERO) < 1) {
			throw new IllegalArgumentException("The amount should be greater than 0.");
		}
		if (trn.getCustomerEmail() != null && !trn.getCustomerEmail().matches(EMAIL_PATTERN)) {
			throw new PatternSyntaxException("Customer emila is invalid sytax.", UUID_PATTERN, -1);
		}
		if (trn.getStatus() != null && !trn.getStatus().equals(TransactionStatus.APPROVED.name())
				&& !trn.getStatus().equals(TransactionStatus.REVERSED.name())
				&& !trn.getStatus().equals(TransactionStatus.REFUNDED.name())
				&& !trn.getStatus().equals(TransactionStatus.ERROR.name())) {
			throw new IllegalArgumentException("The status should be one of: APPROVED, REVERSED, REFUNDED, ERROR.");
		}
	}
}
