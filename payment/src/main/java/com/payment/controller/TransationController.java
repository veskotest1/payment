package com.payment.controller;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.payment.entity.Transaction;
import com.payment.service.TransactionService;

@Controller
@RequestMapping("/trn")
@SuppressWarnings({"PMD", "sonarlintMain"})
public class TransationController {

	@Autowired
	private TransactionService service;

	@GetMapping("/transactions")
	public ResponseEntity<List<Transaction>> getAllTransactions() {
		return ResponseEntity.ok(service.getAllTransactions());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Optional<Transaction>> getTransactionById(@PathVariable UUID id) {
		return ResponseEntity.ok(service.getByUuid(id));
	}

	@PostMapping("/create")
	public ResponseEntity<String> create(@RequestBody Transaction trn) {
		service.create(trn);
		return new ResponseEntity<>("Transaction was created successful", HttpStatus.CREATED);
	}

	@PostMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable UUID id) {
		service.deleteById(id);
		return new ResponseEntity<>("Transaction has been deleted!", HttpStatus.OK);
	}
}
