package com.payment.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.payment.entity.Merchant;
import com.payment.service.MerchantService;

@Controller
@RequestMapping("/merchant")
@SuppressWarnings({"PMD", "sonarlintMain"})
public class MerchantController {

	@Autowired
	private MerchantService service;

	@PostMapping(value = "/create")
	public ResponseEntity<String> create(@RequestBody Merchant merchant) {
		service.create(merchant);
		return new ResponseEntity<>("Merchant was created successful", HttpStatus.CREATED);
	}

	@GetMapping("/merchants")
	public ResponseEntity<List<Merchant>> getAllMerchant() {
		return ResponseEntity.ok(service.getAllMerchants());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Optional<Merchant>> getMerchantById(@PathVariable long id) {
		return ResponseEntity.ok(service.getById(id));
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Optional<Merchant>> update(@RequestBody Merchant merchant, @PathVariable Long id) {
		return ResponseEntity.ok(service.update(merchant, id));
	}

	@PostMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		service.deleteById(id);
		return new ResponseEntity<>("Merchant has been deleted!", HttpStatus.OK);
	}
}
