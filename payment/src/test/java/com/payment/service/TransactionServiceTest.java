package com.payment.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.payment.entity.Transaction;
import com.payment.repository.TransactionRepository;
import com.payment.service.impl.TransactionServiceImpl;

@SuppressWarnings("PMD")
public class TransactionServiceTest {

	private static final UUID TRN_ID = UUID.randomUUID();

	private TransactionRepository repository;
	private TransactionServiceImpl service;

	@BeforeEach
	public void setUp() {
		repository = mock(TransactionRepository.class);
		service = new TransactionServiceImpl();
		service.setRepository(repository);
	}

	@Test
	public void testFindAll() {
		// GIVEN
		List<Transaction> transactions = new ArrayList<>();
		when(repository.findAll()).thenReturn(transactions);

		// WHEN
		List<Transaction> returned = service.getAllTransactions();

		// THEN
		verify(repository, times(1)).findAll();
		assertEquals(transactions, returned);
	}

	@Test
	public void testFindById() {
		// GIVEN
		Optional<Transaction> trn = Optional.of(new Transaction());
		when(repository.findById(TRN_ID)).thenReturn(trn);

		// WHEN
		Optional<Transaction> returned = service.getByUuid(TRN_ID);

		// THEN
		verify(repository, times(1)).findById(TRN_ID);
		verifyNoMoreInteractions(repository);
		assertEquals(trn, returned);
	}

	@Test
	public void testDelete() {
		// GIVEN
		when(repository.findById(TRN_ID)).thenReturn(Optional.of(new Transaction()));

		// WHEN
		service.deleteById(TRN_ID);

		// THEN
		verify(repository, times(1)).deleteById(TRN_ID);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public void deleteOldTrnsTest() {
		// GIVEN
		Transaction trn = new Transaction();
		trn.setId(TRN_ID);
		trn.setPublishDate(new Timestamp(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(3)));
		List<Transaction> transactions = new ArrayList<>();
		transactions.add(trn);
		when(repository.findAll()).thenReturn(transactions);

		// WHEN
		service.deleteOldTrns();

		// THEN
		verify(repository, times(1)).deleteById(TRN_ID);
	}
}
