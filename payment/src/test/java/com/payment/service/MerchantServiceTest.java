package com.payment.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.payment.entity.Merchant;
import com.payment.repository.MerchantRepository;
import com.payment.service.impl.MerchantServiceImpl;

@SuppressWarnings("PMD")
public class MerchantServiceTest {

	private static final Long MERCHANT_ID = Long.valueOf(5);

	private MerchantRepository repository;
	private MerchantServiceImpl service;

	@BeforeEach
	public void setUp() {
		repository = mock(MerchantRepository.class);
		service = new MerchantServiceImpl();
		service.setRepository(repository);
	}

	@Test
	public void testFindAll() {
		// GIVEN
		List<Merchant> merchants = new ArrayList<Merchant>();
		when(repository.findAll()).thenReturn(merchants);

		// WHEN
		List<Merchant> returned = service.getAllMerchants();

		// THEN
		verify(repository, times(1)).findAll();
		assertEquals(merchants, returned);
	}

	@Test
	public void testFindById() {
		// GIVEN
		Optional<Merchant> mer = Optional.of(new Merchant());
		when(repository.findById(MERCHANT_ID)).thenReturn(mer);

		// WHEN
		Optional<Merchant> returned = service.getById(MERCHANT_ID);

		// THEN
		verify(repository, times(1)).findById(MERCHANT_ID);
		verifyNoMoreInteractions(repository);
		assertEquals(mer, returned);
	}

	@Test
	public void testUpdate() {
		// GIVEN
		Merchant updated = new Merchant();
		Merchant old = new Merchant();
		when(repository.findById(updated.getId())).thenReturn(Optional.of(old));

		// WHEN
		service.update(updated, old.getId());

		// THEN
		verify(repository, times(1)).findById(updated.getId());
	}

	@Test
	public void testDelete() {
		// GIVEN
		when(repository.findById(MERCHANT_ID)).thenReturn(Optional.of(new Merchant()));

		// WHEN
		service.deleteById(MERCHANT_ID);

		// THEN
		verify(repository, times(1)).deleteById(MERCHANT_ID);
		verifyNoMoreInteractions(repository);
	}

	@Test
	public void testCreate() {
		// GIVEN
		Merchant merchant = new Merchant();
		when(repository.findById(MERCHANT_ID)).thenReturn(Optional.of(merchant));

		// WHEN
		service.create(merchant);

		// THEN
		verify(repository, times(1)).save(merchant);
		verifyNoMoreInteractions(repository);
	}

}
